# Messaging #

A bunch of useful pieces of messaging infrastructure

### What's this for? ###

I wanted to do in-memory pub/sub, with some reflect-based convenience and process managers

This is the result. It's still a work in progress, but it's already kicking goals for me.

### How does it work? ###

There are a few main bits:

* Bus - you'll need one of these
* Handler - implement one of these to receive and publish messages
* ProcessManagerRunner - a fancy handler. Runs based on a ProcessManagerSpec, and manages correlation between messages and instances of state

There are also some bits designed to augment the above:

* QueuedHandler - puts a buffered chan and a goroutine between producers and your handler
* LoggingHandler - subscribe this fella to some topic on your Bus to see what's arriving there

### Go dependencies ###

None! You take a dependency on this (sorry, we have to have dependencies somewhere)

### Future plans ###

These are features I intend to add:

* ProcessManagerState persistence (injectable)
* Queue persistence (injectable)

### Contributing ###

This is just a little hobby lib for me, but if you really think it's that useful, feel free to send pull requests.

Keep in mind that if this starts getting pull requests, I'm going to start actually unit testing, so you should too :P