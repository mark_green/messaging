package messaging

import (
	"log"
	"reflect"
)

type LoggingHandler struct {
}

func (this LoggingHandler) Handle(message Message, publisher Publisher) {
	log.Println(reflect.TypeOf(message), "::", message)
}