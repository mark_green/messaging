package messaging

import (
	"reflect"
	"strings"
)

type AutoProcessManagerSpecRequirements interface {
	GetCorrelationFor(message Message) string
	NewState(correlation string) ProcessManagerState
	GetProcessName() string
}

type AutoProcessManagerSpec struct {
	requirements AutoProcessManagerSpecRequirements
	handleMethodsByMessageTypeName map[string]reflect.Method
}

func NewAutoProcessManagerSpec(requirements AutoProcessManagerSpecRequirements) (instance *AutoProcessManagerSpec) {
	instance = new(AutoProcessManagerSpec)
	instance.requirements = requirements
	instance.handleMethodsByMessageTypeName = make(map[string]reflect.Method)
	handleMethods := getMethods(requirements, isAutoProcessManagerSpecHandleMethod)
	if len(handleMethods) == 0 {
		panic("No handle methods found on " + reflect.TypeOf(requirements).Name())
	}
	for _, method := range handleMethods {
		instance.handleMethodsByMessageTypeName[method.Type.In(2).Name()] = method
	}
	return
}

func (this *AutoProcessManagerSpec) Handle(state ProcessManagerState, message Message, publisher Publisher) ProcessManagerState {
	handleMethod, exists := this.handleMethodsByMessageTypeName[reflect.TypeOf(message).Name()]
	if exists {
		return handleMethod.Func.Call([]reflect.Value{
			reflect.ValueOf(this.requirements),
			reflect.ValueOf(state),
			reflect.ValueOf(message),
			reflect.ValueOf(publisher),	
		})[0].Interface().(ProcessManagerState)
	}
	return state
}

func (this *AutoProcessManagerSpec) GetCorrelationFor(message Message) string {
	return this.requirements.GetCorrelationFor(message)
}

func (this *AutoProcessManagerSpec) NewState(correlation string) ProcessManagerState {
	return this.requirements.NewState(correlation)
}

func (this *AutoProcessManagerSpec) GetProcessName() string {
	return this.requirements.GetProcessName()
}

func (this *AutoProcessManagerSpec) GetHandledTopics() []string {
	topics := make([]string, 0, len(this.handleMethodsByMessageTypeName))
    for key := range this.handleMethodsByMessageTypeName {
        topics = append(topics, key)
    }
    return topics
}

func isAutoProcessManagerSpecHandleMethod(method reflect.Method) bool {
	return strings.HasPrefix(method.Name, "Handle") &&
			method.Type.NumIn() == 4 &&
			method.Type.NumOut() == 1 &&
			method.Type.In(1).Implements(typeOfProcessManagerState) &&
			method.Type.In(2).Implements(typeOfMessage) &&
			method.Type.In(3) == typeOfPublisher &&
			method.Type.In(1) == method.Type.Out(0) 
}

