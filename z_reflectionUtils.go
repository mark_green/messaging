package messaging

import (
	"reflect"
)

// Precomputed types for .Implements() checks
var typeOfProcessManagerState = reflect.TypeOf((*ProcessManagerState)(nil)).Elem()
var typeOfMessage = reflect.TypeOf((*Message)(nil)).Elem()
var typeOfPublisher = reflect.TypeOf((*Publisher)(nil)).Elem()

func getMethods(obj interface{}, filter func(reflect.Method) bool) []reflect.Method {
	tSpec := reflect.TypeOf(obj)

	out := make([]reflect.Method, 0, tSpec.NumMethod())
	
	for i := 0; i < tSpec.NumMethod(); i++ {
		method := tSpec.Method(i)
		if filter(method) {
			out = append(out, method)
		}
	}

	return out
}