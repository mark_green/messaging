package messaging

type Runnable interface {
	Run(done <-chan struct{}, doneReply chan<- struct{})
}

type CompositeRunnable struct {
	runnables []Runnable
}

func NewCompositeRunnable(runnables... Runnable) (instance *CompositeRunnable) {
	instance = new(CompositeRunnable)
	instance.runnables = runnables
	return
}

func (this *CompositeRunnable) Run(done <-chan struct{}, doneReply chan<- struct{}) {
	doneReply2 := make(chan struct{})
	for _, runnable := range this.runnables {
		go runnable.Run(done, doneReply2)
	}

	// Block until all runnables signal they've finished
	for _ = range this.runnables {
		<- doneReply2
	}

	// Tell our owner we're done
	doneReply <- struct{}{}
}
