package messaging

import (
	"sync"
	"reflect"
)

type Bus struct {
	mutex sync.RWMutex
	multiplexersByTopic map[string]*Multiplexer
}

func NewBus() (instance *Bus) {
	instance = new(Bus)
	instance.multiplexersByTopic = make(map[string]*Multiplexer)
	instance.multiplexersByTopic[""] = NewMultiplexer()
	return
}

func (this *Bus) SubscribeToAll(handler Handler) {
	this.SubscribeTo([]string {""}, handler)
}

func (this *Bus) SubscribeTo(topics []string, handler Handler) {
	this.mutex.Lock()
	for _, topic := range topics {
		multiplexer, exists := this.multiplexersByTopic[topic]
		if !exists {
			multiplexer = NewMultiplexer()
			this.multiplexersByTopic[topic] = multiplexer
		}
		
		multiplexer.AddHandler(handler)
	}
	this.mutex.Unlock()
}

func (this *Bus) AutoSubscribe(autoSubscribers... AutoSubscriber)  {
	for _, subscriber := range autoSubscribers {
		this.SubscribeTo(subscriber.GetHandledTopics(), subscriber)
	}
}

func (this *Bus) Publish(message Message) {
	// Publish to any custom topics, plus empty topic, plus Message type name topic
	topics := append(message.GetTopics(), "", reflect.TypeOf(message).Name())

	for _, topic := range topics {
		this.mutex.RLock()
		multiplexer, exists := this.multiplexersByTopic[topic]
		this.mutex.RUnlock()
		if exists {
			multiplexer.Handle(message, this)
		}
	}
}