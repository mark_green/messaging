package messaging

import (
	"reflect"
	"strings"
)

type AutoHandlerRequirements interface {}

type AutoHandler struct {
	requirements AutoHandlerRequirements
	handleMethodsByMessageTypeName map[string]reflect.Method
}

func NewAutoHandler(requirements AutoHandlerRequirements) (instance *AutoHandler) {
	instance = new(AutoHandler)
	instance.requirements = requirements
	instance.handleMethodsByMessageTypeName = make(map[string]reflect.Method)
	handleMethods := getMethods(requirements, isAutoHandlerHandleMethod)
	if len(handleMethods) == 0 {
		panic("No handle methods found on " + reflect.TypeOf(requirements).Name())
	}
	for _, method := range handleMethods {
		instance.handleMethodsByMessageTypeName[method.Type.In(1).Name()] = method
	}
	return
}

func (this *AutoHandler) Handle(message Message, publisher Publisher) {
	handleMethod, exists := this.handleMethodsByMessageTypeName[reflect.TypeOf(message).Name()]
	if exists {
		handleMethod.Func.Call([]reflect.Value{
			reflect.ValueOf(this.requirements),
			reflect.ValueOf(message),
			reflect.ValueOf(publisher),	
		})
	}
}

func (this *AutoHandler) GetHandledTopics() []string {
	topics := make([]string, 0, len(this.handleMethodsByMessageTypeName))
    for key := range this.handleMethodsByMessageTypeName {
        topics = append(topics, key)
    }
    return topics
}

func isAutoHandlerHandleMethod(method reflect.Method) bool {
	return strings.HasPrefix(method.Name, "Handle") &&
			method.Type.NumIn() == 3 &&
			method.Type.NumOut() == 0 &&
			method.Type.In(1).Implements(typeOfMessage) &&
			method.Type.In(2) == typeOfPublisher
}
