package messaging

type AutoProcessManagerRunner struct {
	spec *AutoProcessManagerSpec
	inner *ProcessManagerRunner
}

func NewAutoProcessManagerRunner(requirements AutoProcessManagerSpecRequirements) *AutoProcessManagerRunner {
	instance := new(AutoProcessManagerRunner)
	instance.spec = NewAutoProcessManagerSpec(requirements)
	instance.inner = NewProcessManagerRunner(instance.spec)
	return instance
}

func (this *AutoProcessManagerRunner) WithPersister(persister ProcessManagerPersister) *AutoProcessManagerRunner {
	this.inner = this.inner.WithPersister(persister)
	return this
}

func (this *AutoProcessManagerRunner) Handle(message Message, publisher Publisher) {
	this.inner.Handle(message, publisher)
}

func (this *AutoProcessManagerRunner) GetHandledTopics() []string {
	return this.spec.GetHandledTopics()
}