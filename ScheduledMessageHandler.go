package messaging

import (
	"sync"
	"time"
)

type SendAt struct {
	Message Message
	DueAt time.Time
}

func (this SendAt) GetTopics() []string {
	return []string{}
}

type scheduledMessage struct {
	request SendAt
	publisher Publisher
}

type ScheduledMessageHandler struct {
	mutex sync.RWMutex
	scheduledMessages []scheduledMessage
	resolution time.Duration
}

func NewScheduledMessageHandler(resolution time.Duration) (instance *ScheduledMessageHandler) {
	instance = new(ScheduledMessageHandler)
	instance.scheduledMessages = make([]scheduledMessage, 0)
	instance.resolution = resolution

	return
}

func (this *ScheduledMessageHandler) Handle(message Message, publisher Publisher) {
	switch message.(type) {
	case SendAt:
		this.mutex.Lock()
		this.scheduledMessages = append(this.scheduledMessages, scheduledMessage {
			request: message.(SendAt),
			publisher: publisher,
		})
		this.mutex.Unlock()
	}
}

func (this *ScheduledMessageHandler) Run(done <-chan struct{}, doneReply chan<- struct{}) {
	ticker := time.NewTicker(this.resolution)

	for quit:=false; !quit; {
		select {
		case now := <-ticker.C:
			this.mutex.Lock()
			remainingScheduledMessages := make([]scheduledMessage, 0, len(this.scheduledMessages))
			for _, scheduledMessage := range this.scheduledMessages {
				if now.After(scheduledMessage.request.DueAt) {
					scheduledMessage.publisher.Publish(scheduledMessage.request.Message)
				} else {
					remainingScheduledMessages = append(remainingScheduledMessages, scheduledMessage)
				}
			}
			this.scheduledMessages = remainingScheduledMessages
			this.mutex.Unlock()
		case <-done:
			ticker.Stop()
			quit = true
		}
	}

	// Tell our owner we're done
	doneReply <- struct{}{}
}

func (this *ScheduledMessageHandler) GetHandledTopics() []string {
	return []string{"SendAt"}
}
