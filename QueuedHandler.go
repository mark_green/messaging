package messaging

type QueuedHandler struct {
	handler Handler
	queue chan handleRequest
}
type handleRequest struct {
	message Message
	publisher Publisher
}

func NewQueuedHandler(handler Handler, queueLength int) (instance *QueuedHandler) {
	instance = new(QueuedHandler)
	instance.handler = handler
	instance.queue = make(chan handleRequest, queueLength)
	return
}

func (this *QueuedHandler) Run(done <-chan struct{}) {
	go func() {
		select {
		case handleRequest := <-this.queue:
			this.handler.Handle(handleRequest.message, handleRequest.publisher)
		case <-done:
			return
		}
	}()
}

func (this *QueuedHandler) Handle(message Message, publisher Publisher) {
	this.queue <- handleRequest {message: message, publisher: publisher}
}
