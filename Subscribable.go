package messaging

type Subscribable interface {
	SubscribeTo(topics []string, handler Handler)
	AutoSubscribe(autoSubscribers... AutoSubscriber)
}

type AutoSubscriber interface {
	Handler
	GetHandledTopics() []string
}