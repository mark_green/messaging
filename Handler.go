package messaging

type Handler interface {
	Handle(message Message, publisher Publisher)
}
