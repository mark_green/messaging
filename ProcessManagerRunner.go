package messaging

import (
	"sync"
)

type ProcessManagerRunner struct {
	mutex sync.RWMutex
	spec ProcessManagerSpec
	stateForCorrelation map[string]ProcessManagerState
	persister ProcessManagerPersister
	putStateJobs chan func()
}

func NewProcessManagerRunner(spec ProcessManagerSpec) (instance *ProcessManagerRunner) {
	instance = new(ProcessManagerRunner)
	instance.spec = spec
	instance.stateForCorrelation = make(map[string]ProcessManagerState)
	instance.persister = noopPersister{}
	instance.putStateJobs = make(chan func())
	go func() {
		for {
			job := <-instance.putStateJobs
			job()
		}
	}()
	return
}

func (this *ProcessManagerRunner) WithPersister(persister ProcessManagerPersister) *ProcessManagerRunner {
	this.persister = persister
	return this
}

func (this *ProcessManagerRunner) Handle(message Message, publisher Publisher) {
	correlation := this.spec.GetCorrelationFor(message)

	this.mutex.Lock()
	state, exists := this.stateForCorrelation[correlation]
	if !exists {
		state = this.persister.GetState(this.spec.GetProcessName(), correlation, this.spec.NewState(correlation))
	}
	state = this.spec.Handle(state, message, publisher)
	this.stateForCorrelation[correlation] = state
	this.putStateJobs <- func() {
		this.persister.PutState(this.spec.GetProcessName(), correlation, state)
	}
	this.mutex.Unlock()
}
