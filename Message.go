package messaging

type Message interface {
	GetTopics() []string
}
