package messaging

type ProcessManagerPersister interface {
	PutState(processManagerType string, correlation string, state ProcessManagerState)
	GetState(processManagerType string, correlation string, state ProcessManagerState) ProcessManagerState
}


type noopPersister struct {}
func (np noopPersister) PutState(processManagerType string, correlation string, state ProcessManagerState) {/*noop*/}
func (np noopPersister) GetState(processManagerType string, correlation string, state ProcessManagerState) ProcessManagerState {
	return state
}