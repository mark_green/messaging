package messaging

import (
	"sync"
)

type Multiplexer struct {
	mutex sync.RWMutex
	handlers []Handler
}

func NewMultiplexer() (instance *Multiplexer) {
	instance = new(Multiplexer)
	instance.handlers = make([]Handler, 0)
	return
}

func (this *Multiplexer) AddHandler(handler Handler) {
	this.mutex.Lock()
	defer this.mutex.Unlock()

	this.handlers = append(this.handlers, handler)
}

func (this *Multiplexer) Handle(message Message, publisher Publisher) {
	this.mutex.RLock()
	defer this.mutex.RUnlock()

	for _, handler := range this.handlers {
		handler.Handle(message, publisher)
	}
}
