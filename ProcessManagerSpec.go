package messaging

type ProcessManagerSpec interface {
	Handle(state ProcessManagerState, message Message, publisher Publisher) ProcessManagerState
	GetCorrelationFor(message Message) string
	NewState(correlation string) ProcessManagerState
	GetProcessName() string
}
